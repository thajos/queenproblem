<?php

// This could be placed in a controller, but I was too lazy for it.
require('QueenProblemView.php');
$queenCalc = new QueenCalcService();
$queenCalc->calculateQueenPositionsWhereNoQueensIntersect();

/**
 * This Service provides functionality to perform calculations on the Queen piece.
 * For now, it can only calculate the possible ways 7 queens can be positioned on a chess board without
 * any possible ways to take another queen in one move.
 */
class QueenCalcService
{

    /**
     * A square chess board containing rows and columns (numbers). On creation no queens are placed, so every cell is empty (0).
     * When a queen is placed, the cell will be filled (1).
     * e.g.: [
     *       [0, 0]
     *       [0, 0]
     * ]
     * @var array
     */
    private $chessBoard;

    /**
     * This will be incremented every time we find a possible solution to the queen problem.
     * After all solutions are found, it will be displayed on the page.
     * @var int
     */
    private $numSolutions = 0;

    /**
     * After solving the problem, I decided to make this a class variable instead of hardcoding 7 everywhere,
     * to play around with different board sizes.
     * @var int
     */
    private $boardSize;

    /**
     * A very basic View component, used to print out the solution.
     * @var QueenProblemView
     */
    private $view;

    /**
     * QueenCalcService constructor. Creates a chessboard and sets the $boardSize variable that will be used throughout this object.
     * @param int $boardSize
     */
    public function __construct($boardSize = 7)
    {
        $this->boardSize = $boardSize;
        $this->chessBoard = $this->createChessBoard($boardSize);

        $this->view = new QueenProblemView($this->chessBoard, $this->boardSize);
    }

    /**
     * Entry point to provide a solution to the queens problem. Creates a chessboard and starts placing queens, from
     * top to bottom. If a placed queen yields no solution, the queen will be removed from the board.
     * When all solutions are found, it renders a HTML page to show them.
     * @return void
     */
    public function calculateQueenPositionsWhereNoQueensIntersect()
    {
        $chessBoard = $this->chessBoard;

        // Note that we give 0 as the starting column.
        $this->placeQueens($chessBoard, 0);

        $this->view->addNumSolutions($this->numSolutions);
        $this->view->render();
    }

    /**
     * Generates simple representation of an empty (0 = no queen placed on the sqaure) square chess board given a row length.
     * @param int $rowLength
     * @return array
     */
    private function createChessBoard($rowLength = 7)
    {
        $chessBoard = [];
        for ($rowNum = 0; $rowNum < $rowLength; $rowNum++) {
            $row = [];
            for ($colNum = 0; $colNum < $rowLength; $colNum++) {
                $row[] = 0;
            }

            $chessBoard[] = $row;
        }

        return $chessBoard;
    }

    /**
     * This checks if a queen can be placed on a certain tile ($board[$rowNum][$colNum].
     * Since we are placing queens from left to right, top to bottom, there are never any queens in the lower side or
     * lower-right side, so we don't need to check those.
     * @param array $board
     * @param int $rowNum
     * @param int $colNum
     * @return bool
     */
    private function isSafe(array $board, $rowNum, $colNum)
    {
        // Check this row on left side for any queens.
        for ($column = $colNum; $column >= 0; $column--) {
            if ($board[$rowNum][$column] === 1) return false;
        }

        // Check the upper diagonal left side from the current position for any queens.
        for ($row = $rowNum, $column = $colNum;
             $row >= 0 && $column >= 0;
             $row--, $column--) {
            if ($board[$row][$column] === 1) return false;
        }

        // This column on the upper side for any queens.
        for ($row = $rowNum; $row >= 0; $row--) {
            if ($board[$row][$colNum] === 1) return false;
        }

        // Check the upper diagonal right side from the current position for any queens.
        for ($row = $rowNum, $column = $colNum;
             $row >= 0 && $column < $this->boardSize;
             $row--, $column++) {
            if ($board[$row][$column] === 1) return false;
        }

        // Check the lower diagonal left side from the current position for any queens.
        for ($row = $rowNum, $column = $colNum;
             $row < $this->boardSize && $column >= 0;
             $row++, $column--) {
            if ($board[$row][$column] === 1) return false;
        }

        // Everything is safe!
        return true;
    }

    /**
     * This method appends the solution to the view.
     * @param array $board
     * @param int $boardSize
     */
    private function createViewForSolution(array $board, $boardSize, $numSolutions)
    {
        $this->view->createSolutionTableForBoard($board, $boardSize, $numSolutions);
    }

    /**
     * This method keeps placing queens on the board recursively, starting from the top-left
     * of the board ($booard[0][0]. It tries placing a queen on all columns in a row. If the queen
     * is safe, it stays there. If there are no safe columns, the queen is removed from the board and
     * we try with the next row. Since this works recursively, if a specific queen placement
     * yields no possible solutions, it will be removed recursively.
     * @param array $board
     * @param int $column
     * @return bool
     */
    private function placeQueens(array $board, $column = 0)
    {
        // We start positioning queens on the top left column. If we get to the final column, all queens are placed.
        if ($column === $this->boardSize) {
            $this->numSolutions++;
            $this->createViewForSolution($board, $this->boardSize, $this->numSolutions);
            return true;
        }

        // We start placing a queen on the first row, first column. Given the column, try placing a queen in all rows one by one.
        for ($row = 0; $row < $this->boardSize; $row++) {
            // Check if queen can be placed on $board[$row][$col].
            if ($this->isSafe($board, $row, $column) === true) {
                // Place a queen on the board with position $board[$row][$column].
                $board[$row][$column] = 1; // 1 represents a queen.

                // Recur to place rest of the queens on the next column.
                $this->placeQueens($board, $column + 1);

                // We get here if there are no solutions for this row. Remove the queen from the board. Since this is
                // called recursively, the method keeps track of every queen and if, say, the 3rd queen is placed and
                // this placement yields no solutions, it will be removed.
                $board[$row][$column] = 0;
            }
        }

        // If a queen can not be placed in any row in the column, return false.
        return false;
    }
}