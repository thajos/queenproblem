Queen problem
=====================
A solution to that age-old conundrum, "How many queens could you place on a chessboard without any of them being able to capture any other?"


#### Requirements
- PHP 7 (theoretically, it will work on 5.5+, but I have not tested this).

### How do I get set up?
- Download or clone the repo.
- Make sure Apache/NGINX is running.
- in your favourite browser, navigate to [http://localhost/queenproblem/QueenProblem.php](http://localhost/queenproblem/QueenProblem.php)

