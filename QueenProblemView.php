<?php

/**
 * This class is responsible for rendering solutions of the queens problem.
 * I created this, because just dumping 0's and 1's onto a page without proper HTML bothers me a lot.
 * Hence, I wanted to create a proper HTML page.
 */
class QueenProblemView
{

    /**
     * Contains the whole HTML page as a string.
     * @var bool|string
     */
    private $html;

    /**
     * This is used to render a queen image. Increments after an image is rendered.
     * @var int
     */
    private $numQueens = 0;

    /**
     * QueenProblemView constructor.
     * @param array $board
     * @param int $boardSize
     */
    public function __construct(array $board, $boardSize)
    {
        $this->html = file_get_contents('./queenView.html');
    }

    /**
     * Creates a HTML table to present a solution to the queen problem and appends it to the page HTML.
     * @param array $board
     * @param int $boardSize
     * @param int $numSolutions
     */
    public function createSolutionTableForBoard(array $board, $boardSize, $numSolutions)
    {
        $this->numQueens = 0;
        $table = "<div class='solution'><p>Oplossing $numSolutions</p><table>";
        for ($row = 0; $row < $boardSize; $row++) {
            $table .= "<tr>";
            for ($column = 0; $column < $boardSize; $column++) {
                if ($board[$row][$column] === 0) {
                    $table .= $this->renderEmptySquare();
                } else {
                    $table .= $this->renderQueen();
                    $this->numQueens++;
                }

            }

            $table .= "</tr>";
        }
        $table .= "</table>";
        $table .= "</div> ";

        //Insert our new table element at the end of the page.
        $this->html = substr_replace($this->html, $table, strpos($this->html, ' </body>') - 7, 0);
    }

    /**
     * Adds HTML to our page that shows the total amount of possible solutions.
     * @param int $numSolutions
     */
    public function addNumSolutions($numSolutions)
    {
        $numSolutionsHtml = "<p>Totaal aantal oplossingen: $numSolutions</p><br/><br/>";
        $this->html = substr_replace($this->html, $numSolutionsHtml, strpos($this->html, '</section>'), 0);
    }
    
    /**
     * Renders the HTML page. Should be called if all solutions are present.
     * @return void
     */
    public function render()
    {
        echo $this->html;
    }
    
    /**
     * This returns an empty HTML table cell. Used when there is no queen in the cell.
     * @return string
     */
    private function renderEmptySquare()
    {
        return "<td> </td>";
    }

    /**
     * Renders a queen in a HTML table cell. The fact that I did here what I did amuses me to no end :)
     * @return string
     */
    private function renderQueen()
    {
        $queenImage = $this->numQueens;
        if ($queenImage > 6) $queenImage = 1;
        return "<td><img src='img/queen$queenImage.jpg' alt='queen'/></td>";
    }
}